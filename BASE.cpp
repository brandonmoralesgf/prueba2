/*
	Name: Bravileo
	Copyright: TecNM
	Author: Leonardo
	Date: 24/05/19 19:56
	Description:Este programa con los valores de los coeficientes de A B C para dos variables y D en caso de que sean 3 variables obtiene el valor de dichas variables.
*/

///////////////////////////////////PRIMERA PARTE INICIALIZACION DE VARIABLES Y ARREGLOS////////////////////////////

//****Se inicia sección de declarativas***//
#include <iostream>
#include <string>
using namespace std;
//****Finaliza sección de declarativas***//


//****Se inicia sección de declaracion de variables***//




string Msj_Solicitud = "Escoje el tipo de ecuacion quieres resolver\n 1.AX+BY=C\n 2.AX+BY+CZ=D\n";// Inicializar el mensaje de solicitud de la variable n
string Msj_SolicitudEcuacion = "Ingresa el valor de la funcion para el coeficiente "; //Inicializar el mensaje de solicitud del promedio de la materia
int Ctrl;                                                                 //Se utiliza Crtl para conocer el tipo de estructura que se eva a tomar
int Tam;                                                                  //La cantidad de incognitas en una funcion
char Coef1[4] = {'A','B','C','D'};                                       //Para usar en la asignacion de coeficientes
float x;                                                                   //Respuesta para x
float y;                                                                   //Respuesta para y
float z;                                                                    //Respuesta para z
//****Finaliza sección de declaracion de variables***


int main()
{//Inicio del programa

//PRIMERA PARTE CONTROL DE LAS FUNCIONES INGRESADAS ASI COMO DE LOS COEFICIENTES//

	cout<<Msj_Solicitud<<endl;//Solicitar el tamaño del arreglo mediante el mensaje de solicitud
		while (!((cin>>Ctrl)&&((Ctrl==1)||(Ctrl==2))))  //Definir los 2 valores que puede tomar nuestra variable de control y que esta sea un numero.
	{
		cin.clear (); //Se borra lo ingresado
		fflush(stdin); //Se limpia lo que quedó
		cout<<Msj_Solicitud<<endl; //Se solicita la variable de control hasta que selecione una correcta por medio del mensaje de solicitud
	}

	switch (Ctrl)//Se relaciona la ecuacion requerida con el tammaño del array
	{
		case 1: Tam=3; break;   
		case 2: Tam=4; break;
	}
	
	float Ecuacion1[Tam]; //Se inicializan los arreglos que tomaran los valores de las 2 ecuaciones
	float Ecuacion2[Tam];
	float Ecuacion3[Tam];
		
	for (int i=0; i<Tam; i++) //Repetir la asignacion hasta terminar con los valores de la ecuacion 1
	{	
     	cout<<Msj_SolicitudEcuacion<<"1"<<Coef1[i]<<endl;//Se Solicita el valor A,B,C y D(Si es el caso) para la funcion 1
	     while (!(cin>>Ecuacion1[i])) //Se verifica que el valor ingresado sea un numero
	    {
		cin.clear (); //Se borra lo ingresado
		fflush(stdin); //Se limpia lo que quedó
        }
	}
	
	for (int i=0; i<Tam; i++) //Repetir la asignacion hasta terminar con los valores de la ecuacion 2
	{		
	    cout<<Msj_SolicitudEcuacion<<"2"<<Coef1[i]<<endl;//Se Solicita el valor A,B,C y D(Si es el caso) para la funcion 2
	     while (!(cin>>Ecuacion2[i]))    //Se verifica que el valor ingresado sea un numero
	    {
		cin.clear (); //Se borra lo ingresado
		fflush(stdin); //Se limpia lo que quedó
	    }
    }


return 0;
}
    ////////////////////////////TERMINA LA PRIMERA PARTE DEL PROGRAMA///////////////////////////////
